![](showcase.png)
<h3 align="center">Movie Watchers Android</h3>
<p align="center">
  <p>
    <strong>Projet Android</strong>
    <br />  
    - L'application est développé en Java.
    <br />
    - La base de données est gérée grace aux Android Rooms. (1 relation One-To-Many et 1 relation Many-To-Many)
    <br/> 
    - La recherche de film utilise l'API de themoviedb
    <br />
    <!-- <a href="https://gitlab.com/meunier.jeason/moviewatchersapp"><strong>Explore the docs »</strong></a> -->
    <br />
    <br />
    <p align="center">
        <a href="https://gitlab.com/meunier.jeason/moviewatchersapp/issues">Report Bug</a>
        ·
        <a href="https://gitlab.com/meunier.jeason/moviewatchersapp/issues">Request Feature</a>
    </p>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Sommaire</h2></summary>
  <ol>
    <li>
      <a href="#a-propos-du-projet">A propos du projet</a>
      <ul>
        <li><a href="#outils-utilisés">Outils utilisés</a></li>
      </ul>
    </li>
    <li>
      <a href="#démarrage">Démarrage</a>
      <ul>
        <li><a href="#pré-requis">Pré-requis</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#utilisation">Utilisation</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## A propos du projet

<!-- [![Product Name Screen Shot][product-screenshot]](https://example.com) -->
Ce travail est le résultat du projet Android de la licence PRO WEB & MOBILE d'Orléans:

Le but est de développer une petite application avec les prérequis suivants:
* Utilisation d’Intent pour passer entre les activités
* Utilsition de RecyclerView
* Utilisation de BDD (Room et relations)
* Utilisation d'une API


>**L'apk est disponible [ici](MovieWatchersApp.apk)**


### Outils utilisés

* [Android Java](https://developer.android.com/docs)
* [Room](https://developer.android.com/training/data-storage/room)
* [Api TMDB](https://developers.themoviedb.org/3)


Fragments/Activity:
* La base de l'application fonctionne à l'aide de Fragments. 
* Des activités sont utilisés pour la page de détails des films, des acteurs et la page contenant les films de chaque catégorie(Intents et extras).


Les relations:
* Les acteurs sont liés aux films à l'aide d'une relation N-to-M.
* Les films sont liés aux catégories à l'aide d'une relation One-to-N


<!-- GETTING STARTED -->
## Démarrage

Afin de déployer localement une copie de ce projet, il suffit de cloner ce repo:
   ```sh
   git clone https://gitlab.com/meunier.jeason/moviewatchersapp.git
   ```

Il est possible d'inclure votre propre clé d'API TMDB dans le fichier /app/src/main/res/values/strings.xml:
  ```sh
    # app/src/main/res/values/strings.xml
    # Par défaut, nous avons laisser notre clé d'API pour vos tests
        <string name="apikey">VOTRE_CLE_API</string>
  ```

L'application est maitenant prête à fonctionner! Vous pouvez ouvrir ce projet dans Android Studio.


## Utilisation

Depuis le menu "Drawer", vous pouvez:
* Acceder à l'accueil.
* Acceder à vos favoris.
* Acceder aux acteurs jouant dans vos films favoris.
* Effectuer des recherches par genre ou par année dans l'API TMDB.
* Acceder et gérer vos catégories.

La page d'accueil affiche par défaut des films populaires. Depuis cette page vous pouvez:
* Voir les films populaires du moment.
* Rechercher un film par son titre/synopsis dans l'API TMDB.
* Acceder aux détails d'un film en appuyant dessus.

La page détails d'un film:
* Cette page affiche le titre, la date de sortie, les genres et le synopsis du film.
* Vous trouverez aussi les 10 acteurs principaux du film.
* Si un Trailer est disponible, il sera possible de le visionner.
* En appuyant sur le coeur, vous pourrez ajouter un film a vos favoris en choisissant une catégorie.

La page "Mes Favoris":
* Cette page affiche tous les films compris dans vos favoris
* Vous pouvez chercher un film à l'aide de son titre
* Tout comme sur la page d'accueil,vous pouvez acceder à la page de détails de vos films.
* Vous pouvez supprimer un film de vos favoris.

La page "Mes Acteurs":
* Cette page liste les acteurs principaux jouant dans vos films favoris.
* Vous pouvez chercher un acteur à l'aide de son nom/prénom
* Acceder aux détails d'un acteur en appuyant dessus.

La page détails d'un acteur:
* Cette page affiche la liste de vos films favoris dans lesquels joue l'acteur
* Vous retrouverez aussi la filmographie de l'acteur.

Les pages de recherche (par genre / par année):
* La liste de genre est retournée par l'API
* Pour les années, vous pouvez chercher de l'an 1885 à aujourd'hui
* Comme pour les autres pages affichant des films, vous pouvez acceder aux détails en appuyant sur un film

La page "Ajouter une catégorie":
* Simple Formulaire permettant la création d'une catégorie pour mieux trier vos favoris.

La page "Mes catégories":
* Vous retrouverez toutes vos catégories ici, avec la possibilité de les supprimer
* En appuyant sur une catégorie, vous retrouverez vos films favoris qui y sont associés.

## Développeurs
Axel Malsagne / Jeason Meunier

Lien du projet: [https://gitlab.com/meunier.jeason/moviewatchersapp](https://gitlab.com/meunier.jeason/moviewatchersapp)
