package com.example.moviewatchers;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviewatchers.R;
import com.example.moviewatchers.adapter.FavoritesAdapter;
import com.example.moviewatchers.adapter.FilmAdapter;
import com.example.moviewatchers.models.FilmDetails;
import com.example.moviewatchers.viewmodel.FavoritesViewModel;
import com.example.moviewatchers.viewmodel.FilmViewModel;
import com.example.moviewatchers.viewmodel.OwnCategoryViewModel;

public class FavoritesByCategoryActivity extends AppCompatActivity implements FavoritesAdapter.OnDeleteClickListener {
    private FavoritesAdapter favoritesAdapter;
    private OwnCategoryViewModel ownCategoryViewModel;
    private SearchView searchView;
    private int category_id;
    private TextView tvError;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_details);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        ownCategoryViewModel = new ViewModelProvider(this).get(OwnCategoryViewModel.class);
        favoritesAdapter = new FavoritesAdapter(this,this);
        RecyclerView recyclerView = findViewById(R.id.recyclerviewFavorites);
        searchView = findViewById(R.id.searchActor);
        recyclerView.setAdapter(favoritesAdapter);
        tvError = findViewById(R.id.tvError);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        category_id = getIntent().getExtras().getInt("category_id");
        String category_name = getIntent().getExtras().getString("category_name");
        toolbar.setTitle(category_name);
        Log.d("MES LOGS", ""+category_id);
        ownCategoryViewModel.getFilmsWithCategories(category_id).observe(this, ownCategoryWithFilmList -> {
            favoritesAdapter.setFavFilms(ownCategoryWithFilmList.get(0).filmDetailsList);
            if (favoritesAdapter.getItemCount() == 0) {
                tvError.setText("Aucun film dans cette catégorie");
            }else{
                tvError.setText("");
            }
        });

    }

    // TOOLBAR MENU
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Si on clique sur un icon de l'appbar close l'activity
        finish();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void OnDeleteClickListener(FilmDetails film) {
        //Delete
        Log.d("Mes LOGS DE SUPPRESSION", "Je supprime le film: "+film);
        FilmViewModel favoritesFilmViewModel = new ViewModelProvider(this).get(FilmViewModel.class);
        FavoritesViewModel favoritesViewModel = new ViewModelProvider(this).get(FavoritesViewModel.class);
        favoritesFilmViewModel.delete(film);
        favoritesViewModel.deletebyTmdbId(film.getId());

    }
}
