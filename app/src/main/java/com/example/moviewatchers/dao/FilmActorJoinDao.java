package com.example.moviewatchers.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.moviewatchers.models.ActorFilmCount;
import com.example.moviewatchers.models.FilmActorJoin;
import com.example.moviewatchers.models.FilmDetails;

import java.util.List;

@Dao
public interface FilmActorJoinDao {
    @Insert
    void insert(FilmActorJoin filmActorJoin);

    @Delete
    void delete(FilmActorJoin filmActorJoin);

    @Query("SELECT * FROM FilmDetails INNER JOIN film_actor_join ON FilmDetails.id=film_actor_join.film_id")
    LiveData<List<FilmActorJoin>> getAll();

    @Query("SELECT * FROM FilmDetails INNER JOIN film_actor_join ON FilmDetails.id=film_actor_join.film_id WHERE film_actor_join.actor_id=:actorId")
    List<FilmDetails> getFilmsForActorId(final int actorId);

    //@Query("SELECT * FROM Actor INNER JOIN film_actor_join ON Actor.id=film_actor_join.actor_id WHERE film_actor_join.film_id=:filmId")
    //List<FilmDetails> getActorForFilmId(final int filmId);

    @Query("SELECT Actor.id as actor_id, Actor.name as actor_name, Actor.profile_url as actor_profile ,COUNT(title) as nbfilms FROM Actor, FilmDetails INNER JOIN film_actor_join ON Actor.id=film_actor_join.actor_id AND FilmDetails.id = film_actor_join.film_id GROUP BY Actor.name ORDER BY name ASC")
    LiveData<List<ActorFilmCount>> getCountByActor();

}
