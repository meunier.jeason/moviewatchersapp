package com.example.moviewatchers.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.moviewatchers.models.FilmDetails;
import com.example.moviewatchers.models.OwnCategory;
import com.example.moviewatchers.models.OwnCategoryWithFilm;

import java.util.List;

@Dao
public interface OwnCategoryDao {

    @Insert
    void insert(OwnCategory category);

    @Insert( onConflict = OnConflictStrategy.IGNORE)
    void insertFilm(FilmDetails filmDetails);

    @Query("SELECT * FROM OwnCategory")
    LiveData<List<OwnCategory>> getAllCategories();

    @Transaction
    @Query("SELECT * FROM OwnCategory WHERE id = :category_id")
    LiveData<List<OwnCategoryWithFilm>> getFilmsWithCategories(int category_id);

    @Delete
    int delete(OwnCategory category);
}
