package com.example.moviewatchers.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.moviewatchers.models.Actor;
import com.example.moviewatchers.models.FilmDetails;

import java.util.List;

@Dao
public interface ActorDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Actor actor);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAllActors(Actor... actors);

    @Query("SELECT * FROM Actor")
    LiveData<List<Actor>> getAllActors();

    @Query("SELECT * FROM Actor WHERE id=:id LIMIT 1")
    LiveData<Actor> getActorById(int id);

    @Delete
    int delete(Actor actor);
}
