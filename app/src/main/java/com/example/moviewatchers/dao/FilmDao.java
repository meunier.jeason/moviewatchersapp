package com.example.moviewatchers.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.moviewatchers.models.FilmDetails;

import java.util.List;

@Dao
public interface FilmDao {

    @Insert
    void insert(FilmDetails film);

    @Query("SELECT * FROM FilmDetails")
    LiveData<List<FilmDetails>> getAllFavoritesFilms();

    @Query("SELECT * FROM FilmDetails WHERE id=:id_tmdb")
    LiveData<FilmDetails> getFilmById(int id_tmdb);

    @Delete
    int delete(FilmDetails film);
}
