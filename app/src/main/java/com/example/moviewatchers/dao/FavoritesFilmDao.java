package com.example.moviewatchers.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.moviewatchers.models.FavoritesFilm;

import java.util.List;

@Dao
public interface FavoritesFilmDao {

    @Insert
    void insert(FavoritesFilm film);

    @Query("SELECT * FROM favorites")
    LiveData<List<FavoritesFilm>> getAllFavorites();

    @Query("SELECT * FROM favorites WHERE id_tmdb=:id_tmdb")
    LiveData<FavoritesFilm> getByTmdbId(int id_tmdb);

    @Delete
    int delete(FavoritesFilm favoritesFilm);

    @Query("DELETE FROM favorites WHERE id_tmdb = :id")
    int deleteByTmdbId(int id);

}
