package com.example.moviewatchers;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviewatchers.adapter.FilmAdapter;
import com.example.moviewatchers.models.Film;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;


public class SearchYearFragment extends Fragment {
    private List<Film> filmList = new ArrayList<>();
    private FilmAdapter filmAdapter = new FilmAdapter(filmList);
    private LinearLayoutManager linearLayoutManager;
    private int page=1;
    private int maxPage;
    private RecyclerView recyclerView;
    private int year = Calendar.getInstance().get(Calendar.YEAR); // par defaut genre action
    private TextView tvYear;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return inflater.inflate(R.layout.homefragment,container,false);


        View view = inflater.inflate(R.layout.searchyearfragment, container, false);
        recyclerView = view.findViewById(R.id.recyclerviewFilms);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(filmAdapter);
        findYear(recyclerView,1, year);
        // Scroll continu avec appel a l'api
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == filmList.size()-1){
                    if (maxPage == 0 || page < maxPage){
                        page = page + 1;
                        findYear(recyclerView,page,year);


                    }

                }
            }
        });

        tvYear = view.findViewById(R.id.selectedYear);
        tvYear.setText(""+year);
        Button button = view.findViewById(R.id.buttonYear);
        button.setOnClickListener(v -> {
            loadModal();
        });
        return view;

    }

    private void loadModal() {
        NumberPicker numberPicker = new NumberPicker(getActivity());
        numberPicker.setMaxValue(Calendar.getInstance().get(Calendar.YEAR));
        numberPicker.setMinValue(1885);
        numberPicker.setValue(year);


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(numberPicker);
        builder.setTitle("Choisir une année");
        //builder.setMessage("Choose a value :");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                page = 1;
                filmList.clear();
                year = numberPicker.getValue();
                linearLayoutManager.scrollToPositionWithOffset(0, 0);
                tvYear.setText(""+year);
                findYear(recyclerView,page,year);
                Log.d("MES LOGS", ""+year);
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                //
            }
        });
        builder.create();
        builder.show();
    }

    public void findYear(View view, int page, int year){
        URL url = createURL("discover/movie","sort_by=popularity.desc&include_adult=false&include_video=false&page="+page+"&primary_release_year="+year);
        if(url != null){
            GetFilmTask getFilmTask = new GetFilmTask(getActivity());
            getFilmTask.execute(url);
        } else {
            Toast.makeText(getActivity(), "Url Invalide", Toast.LENGTH_LONG).show();
        }
    }

    private URL createURL(String endpoint, String searchFilters) {
        String apiKey = getString(R.string.apikey);
        String baseUrl = getString(R.string.apiurl);

        try{
            String urlString = baseUrl + endpoint +"?api_key="+apiKey+"&language=fr-FR&"+searchFilters;
            Log.d("MesLogs",urlString);
            return new URL(urlString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private class GetFilmTask extends AsyncTask<URL,Void, JSONObject> {
        private Context context;

        public GetFilmTask(Context c){
            context = c;
        }


        @Override
        protected JSONObject doInBackground(URL... params) {
            HttpsURLConnection connection = null;
            try {
                connection = (HttpsURLConnection)params[0].openConnection();
                connection.setConnectTimeout(5000);
                int res = connection.getResponseCode();
                if (res == HttpsURLConnection.HTTP_OK){
                    StringBuilder builder = new StringBuilder();
                    try(BufferedReader reader = new BufferedReader(
                            new InputStreamReader(connection.getInputStream())
                    )){
                        String line;
                        while ((line = reader.readLine()) != null){
                            builder.append(line);
                        }
                    } catch (IOException e){
                        e.printStackTrace();
                    }
                    return new JSONObject(builder.toString());
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject film){
            if(film != null){
                convertJsonToArrayList(film);
                filmAdapter.notifyDataSetChanged();
            }

        }

        private void convertJsonToArrayList(JSONObject film) {
            try{
                JSONArray list = film.getJSONArray("results");
                //Log.d("MesLogs", list.toString());
                for (int i=0; i<list.length();i++){
                    JSONObject filmInfo = list.getJSONObject(i);
                    //Log.d("MesLogs",filmInfo.toString());
                    int id = filmInfo.getInt("id");
                    String title = filmInfo.getString("title");
                    String overview = filmInfo.getString("overview");
                    String posterPath = filmInfo.getString("poster_path");
                    filmList.add(new Film(
                            id,
                            title,
                            overview,
                            posterPath
                    ));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

}
