package com.example.moviewatchers;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviewatchers.adapter.FavoritesAdapter;
import com.example.moviewatchers.adapter.OwnCategoryAdapter;
import com.example.moviewatchers.models.FilmDetails;
import com.example.moviewatchers.models.OwnCategory;
import com.example.moviewatchers.viewmodel.FavoritesViewModel;
import com.example.moviewatchers.viewmodel.FilmViewModel;
import com.example.moviewatchers.viewmodel.OwnCategoryViewModel;

import java.util.List;


public class ManageCategoryFragment extends Fragment implements OwnCategoryAdapter.OnDeleteClickListener {
    private RecyclerView recyclerView;
    private OwnCategoryViewModel ownCategoryViewModel;
    private OwnCategoryAdapter ownCategoryAdapter;
    private TextView tvError;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return inflater.inflate(R.layout.homefragment,container,false);

        View view = inflater.inflate(R.layout.managecategoryfragment, container, false);

        ownCategoryViewModel = new ViewModelProvider(this).get(OwnCategoryViewModel.class);
        ownCategoryAdapter = new OwnCategoryAdapter(getContext(),this);
        recyclerView = view.findViewById(R.id.recyclerviewCategory);
        tvError = view.findViewById(R.id.tvError);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(ownCategoryAdapter);


        LiveData<List<OwnCategory>> ownCategory = ownCategoryViewModel.getAllCategories();
        ownCategory.observe(getViewLifecycleOwner(), ownCategories -> {
                Log.d("DANS LE FRAGMENT",""+ownCategories);
                ownCategoryAdapter.setCategories(ownCategories);
                if (ownCategoryAdapter.getItemCount() == 0) {
                    tvError.setText("Aucune catégorie!");
                }else{
                    tvError.setText("");
                }
        });

        return view;

    }

    @Override
    public void OnDeleteClickListener(OwnCategory category) {
        //Delete
        ownCategoryViewModel.delete(category);
    }

}
