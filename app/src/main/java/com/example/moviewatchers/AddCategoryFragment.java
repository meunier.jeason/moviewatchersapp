package com.example.moviewatchers;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviewatchers.adapter.FilmAdapter;
import com.example.moviewatchers.models.Film;
import com.example.moviewatchers.models.OwnCategory;
import com.example.moviewatchers.viewmodel.OwnCategoryViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;


public class AddCategoryFragment extends Fragment {
    private EditText tvName;
    private EditText tvDesc;
    private TextView tvError;
    private TextView tvSuccess;
    private Button btnAdd;
    private OwnCategoryViewModel ownCategoryViewModel;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return inflater.inflate(R.layout.homefragment,container,false);

        View view = inflater.inflate(R.layout.addcategoryfragment, container, false);
        ownCategoryViewModel = new ViewModelProvider(this).get(OwnCategoryViewModel.class);


        tvName = view.findViewById(R.id.edtNameCat);
        tvDesc = view.findViewById(R.id.edtDescCat);
        tvError = view.findViewById(R.id.tvErrorCat);
        tvSuccess = view.findViewById(R.id.tvValidCat);
        btnAdd = view.findViewById(R.id.btnAddCategory);
        btnAdd.setOnClickListener(v -> {
            String name = tvName.getText().toString();
            String description = tvDesc.getText().toString();
            if(name.matches("") || description.matches("")){
                tvSuccess.setText("");
                tvError.setText("Merci de remplir les 2 champs");
            } else{
                tvError.setText("");
                ownCategoryViewModel.insert(new OwnCategory(name,description));
                tvSuccess.setText("Catégorie ajouté!!");
                tvName.setText("");
                tvDesc.setText("");
                new android.os.Handler().postDelayed(
                        () -> tvSuccess.setText(""), 3000);
            }
        });
        return view;

    }

}
