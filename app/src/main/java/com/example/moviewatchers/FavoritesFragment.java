package com.example.moviewatchers;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviewatchers.adapter.FavoritesAdapter;
import com.example.moviewatchers.models.FilmDetails;
import com.example.moviewatchers.viewmodel.FavoritesViewModel;
import com.example.moviewatchers.viewmodel.FilmViewModel;


public class FavoritesFragment extends Fragment implements FavoritesAdapter.OnDeleteClickListener {
    //private List<FilmDetails> favoritesFilms = new ArrayList<>();
    private FavoritesAdapter favoritesAdapter;
    private FavoritesViewModel favoritesViewModel;
    private FilmViewModel favoritesFilmViewModel;
    private SearchView searchView;
    private TextView tvError;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.favoritesfragment, container, false);
        favoritesFilmViewModel = new ViewModelProvider(this).get(FilmViewModel.class);
        favoritesViewModel = new ViewModelProvider(this).get(FavoritesViewModel.class);
        favoritesAdapter = new FavoritesAdapter(getContext(),this);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerviewFavorites);
        searchView = view.findViewById(R.id.searchActor);
        tvError = view.findViewById(R.id.tvError);

        recyclerView.setAdapter(favoritesAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        Log.d("MESLOGS",favoritesFilmViewModel.getAllFavoritesFilms().toString());
        favoritesFilmViewModel.getAllFavoritesFilms().observe(getViewLifecycleOwner(), filmDetails -> {
            favoritesAdapter.setFavFilms(filmDetails);
            if (favoritesAdapter.getItemCount() == 0) {
                tvError.setText("Aucun film dans vos favoris");
            }else{
                tvError.setText("");
            }
        });



        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                favoritesAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return view;

        //return inflater.inflate(R.layout.favoritesfragment,container,false);
    }

    @Override
    public void OnDeleteClickListener(FilmDetails film) {
        //Delete
        favoritesFilmViewModel.delete(film);
        favoritesViewModel.deletebyTmdbId(film.getId());

    }


}
