package com.example.moviewatchers.models;


import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Relation;

import java.util.List;

import static androidx.room.ForeignKey.CASCADE;

public class OwnCategoryWithFilm {
    @Embedded public OwnCategory ownCategory;
    @Relation(
            parentColumn = "id",
            entityColumn = "own_category_id",
            entity = FilmDetails.class
    )
    public List<FilmDetails> filmDetailsList;
}
