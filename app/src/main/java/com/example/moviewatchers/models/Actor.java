package com.example.moviewatchers.models;


import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class Actor {
    @PrimaryKey
    private int id;
    @Ignore
    private int id_film;
    private String name;
    private String profile_url;

    public Actor(int id, String name, String profile_url){
        this.id = id;
        this.name = name;
        if(profile_url.startsWith("http")) {
            this.profile_url = profile_url;
        }else{
            this.profile_url = "https://image.tmdb.org/t/p/w500" + profile_url;
        }
    }
    public Actor(int id, int id_film, String name, String profile_url){
        this.id = id;
        this.id_film = id_film;
        this.name = name;
        if(profile_url.startsWith("http")) {
            this.profile_url = profile_url;
        }else{
            this.profile_url = "https://image.tmdb.org/t/p/w500" + profile_url;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_film() {
        return id_film;
    }

    public void setId_film(int id_film) {
        this.id_film = id_film;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_url() {
        return profile_url;
    }

    public void setProfile_url(String profile_url) {
        this.profile_url = profile_url;
    }
}
