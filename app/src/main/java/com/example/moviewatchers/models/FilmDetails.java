package com.example.moviewatchers.models;


import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class FilmDetails {
    @PrimaryKey
    private int id;

    private int own_category_id;
    private String title;
    private String overview;
    private String date;
    private String genres;
    private String backdropURL;
    private String posterURL;

    public FilmDetails(Integer id, int own_category_id ,String title, String overview,String date, String genres, String backdropURL, String posterURL){
        this.id = id;
        this.own_category_id = own_category_id;
        this.title = title;
        this.overview = overview;
        this.date = date;
        this.genres = genres;
        if(backdropURL.startsWith("http")){
            this.backdropURL = backdropURL;
        }else {
            this.backdropURL = "https://image.tmdb.org/t/p/w500" + backdropURL;
        }
        if(posterURL.startsWith("http")){
            this.posterURL = posterURL;
        }else {
            this.posterURL = "https://image.tmdb.org/t/p/w500" + posterURL;
        }
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOwn_category_id() {
        return own_category_id;
    }

    public void setOwn_category_id(int own_category_id) {
        this.own_category_id = own_category_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getBackdropURL() {
        return backdropURL;
    }

    public void setBackdropURL(String backdropURL) {
        this.backdropURL = backdropURL;
    }

    public String getPosterURL() {
        return posterURL;
    }

    public void setPosterURL(String posterURL) {
        this.posterURL = posterURL;
    }
}
