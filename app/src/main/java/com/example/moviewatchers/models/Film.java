package com.example.moviewatchers.models;


public class Film {

    private int id;
    private String title;
    private String overview;
    private String imageURL;

    public Film(Integer id, String title, String overview, String imageURL){
        this.id = id;
        this.title = title;
        this.overview = overview;
        this.imageURL = "https://image.tmdb.org/t/p/w500"+imageURL;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
