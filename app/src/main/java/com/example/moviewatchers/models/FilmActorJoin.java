package com.example.moviewatchers.models;


import androidx.room.Delete;
import androidx.room.Entity;
import androidx.room.ForeignKey;

import com.example.moviewatchers.models.Actor;
import com.example.moviewatchers.models.FilmDetails;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "film_actor_join",
        primaryKeys = {"film_id","actor_id"},
        foreignKeys = {
            @ForeignKey(entity = FilmDetails.class,
                        parentColumns = "id",
                        childColumns = "film_id", onDelete = CASCADE),
            @ForeignKey(entity = Actor.class,
                        parentColumns = "id",
                        childColumns = "actor_id", onDelete = CASCADE)
        })

public class FilmActorJoin {
    public final int film_id;
    public final int actor_id;

    public FilmActorJoin(final int film_id, final int actor_id){
        this.film_id = film_id;
        this.actor_id = actor_id;
    }
}
