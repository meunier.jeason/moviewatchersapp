package com.example.moviewatchers.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;

public class ActorFilmCount implements Parcelable {
    @ColumnInfo(name = "actor_id")
    public int actor_id;

    @ColumnInfo(name = "actor_name")
    public String actor_name;

    @ColumnInfo(name = "actor_profile")
    public String actor_profile;

    @ColumnInfo(name = "nbfilms")
    public int nbfilms;


    public ActorFilmCount(int actor_id, String actor_name, String actor_profile, int nbfilms) {
        this.actor_id = actor_id;
        this.actor_name = actor_name;
        this.actor_profile = actor_profile;
        this.nbfilms = nbfilms;
    }


    protected ActorFilmCount(Parcel in) {
        actor_id = in.readInt();
        actor_name = in.readString();
        actor_profile = in.readString();
        nbfilms = in.readInt();
    }

    public static final Creator<ActorFilmCount> CREATOR = new Creator<ActorFilmCount>() {
        @Override
        public ActorFilmCount createFromParcel(Parcel in) {
            return new ActorFilmCount(in);
        }

        @Override
        public ActorFilmCount[] newArray(int size) {
            return new ActorFilmCount[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(actor_id);
        dest.writeString(actor_name);
        dest.writeString(actor_profile);
        dest.writeInt(nbfilms);
    }
}
