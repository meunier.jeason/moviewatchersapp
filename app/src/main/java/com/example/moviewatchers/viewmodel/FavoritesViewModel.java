package com.example.moviewatchers.viewmodel;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.moviewatchers.FavoritesFilmRoomDatabase;
import com.example.moviewatchers.dao.FavoritesFilmDao;
import com.example.moviewatchers.models.FavoritesFilm;

import java.util.List;

public class FavoritesViewModel extends AndroidViewModel {
    private FavoritesFilmDao favoritesFilmDao;
    private FavoritesFilmRoomDatabase favoritesFilmDB;
    private LiveData<List<FavoritesFilm>> allFavorites;

    public FavoritesViewModel(Application application) {
        super(application);
        favoritesFilmDB = FavoritesFilmRoomDatabase.getDatabase(application);
        favoritesFilmDao = favoritesFilmDB.favoritesFilmDao();
        allFavorites = favoritesFilmDao.getAllFavorites();
    }

    public void insert(FavoritesFilm film){
        new InsertAsyncTask(favoritesFilmDao).execute(film);
    }

    public void delete(FavoritesFilm film) { new DeleteAsyncTask(favoritesFilmDao).execute(film);}
    public void deletebyTmdbId(int tmdb_id) {
        Log.d("MES LOGS", "JE SUIS DANS LE FAVORITE VIEW HOLDER");
        new DeleteByTmdbIdAsyncTask(favoritesFilmDao).execute(tmdb_id);
    }

    LiveData<List<FavoritesFilm>> getAllFavorites(){
        return allFavorites;
    }

    public LiveData<FavoritesFilm> getByTmdbId(int tmdb_id){
        return favoritesFilmDao.getByTmdbId(tmdb_id);
    }


    private class OperationsAsyncTask extends AsyncTask<FavoritesFilm, Void, Void> {
        FavoritesFilmDao asyncFavDao;

        OperationsAsyncTask(FavoritesFilmDao favoritesFilmDao){
            this.asyncFavDao = favoritesFilmDao;
        }

        @Override
        protected Void doInBackground(FavoritesFilm... favoritesFilms) {
            return null;
        }
    }


    private class InsertAsyncTask extends OperationsAsyncTask {

        InsertAsyncTask(FavoritesFilmDao favoritesFilmDao) {super(favoritesFilmDao);}

        @Override
        protected Void doInBackground(FavoritesFilm... favoritesFilms) {
            asyncFavDao.insert(favoritesFilms[0]);
            return null;
        }
    }

    private class DeleteAsyncTask extends OperationsAsyncTask{

        DeleteAsyncTask(FavoritesFilmDao favoritesFilmDao) {super(favoritesFilmDao);}

        @Override
        protected Void doInBackground(FavoritesFilm... favoritesFilms) {
            asyncFavDao.delete(favoritesFilms[0]);
            return null;
        }
    }

    private class DeleteByTmdbIdAsyncTask extends AsyncTask<Integer, Void, Void> {
        FavoritesFilmDao asyncFavDao;
        DeleteByTmdbIdAsyncTask(FavoritesFilmDao favoritesFilmDao){
            this.asyncFavDao = favoritesFilmDao;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            asyncFavDao.deleteByTmdbId(integers[0]);
            return null;
        }
    }


    @Override
    protected void onCleared(){
        super.onCleared();
        Log.i("MONVIEWMODEL","ViewModel DETRUIT");
    }
}
