package com.example.moviewatchers.viewmodel;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.moviewatchers.FavoritesFilmRoomDatabase;
import com.example.moviewatchers.dao.ActorDao;
import com.example.moviewatchers.models.Actor;

import java.util.List;

public class ActorViewModel extends AndroidViewModel {
    private ActorDao actorDao;
    private FavoritesFilmRoomDatabase favoritesFilmDB;
    private LiveData<List<Actor>> allActors;

    public ActorViewModel(Application application) {
        super(application);
        favoritesFilmDB = FavoritesFilmRoomDatabase.getDatabase(application);
        actorDao = favoritesFilmDB.actorDao();
        allActors = actorDao.getAllActors();
    }

    public void insert(Actor actor){
        new InsertAsyncTask(actorDao).execute(actor);
    }

    public void delete(Actor actor) { new DeleteAsyncTask(actorDao).execute(actor);}

    LiveData<List<Actor>> getAllActors(){
        return allActors;
    }

    public LiveData<Actor> getActorById(int actor_id){
        return actorDao.getActorById(actor_id);
    }


    private class OperationsAsyncTask extends AsyncTask<Actor, Void, Void> {
        ActorDao asyncFavDao;

        OperationsAsyncTask(ActorDao actorDao){
            this.asyncFavDao = actorDao;
        }

        @Override
        protected Void doInBackground(Actor... actors) {
            return null;
        }
    }


    private class InsertAsyncTask extends OperationsAsyncTask {

        InsertAsyncTask(ActorDao actorDao) {super(actorDao);}

        @Override
        protected Void doInBackground(Actor... actors) {
            asyncFavDao.insert(actors[0]);
            return null;
        }
    }



    private class DeleteAsyncTask extends  OperationsAsyncTask {

        DeleteAsyncTask(ActorDao actorDao) {super(actorDao);}

        @Override
        protected Void doInBackground(Actor... actors) {
            Log.i("DELETE UN FILM","JE SUIS DANS LE ONBACKGROUND DU DELETE");
            asyncFavDao.delete(actors[0]);
            return null;
        }
    }




    @Override
    protected void onCleared(){
        super.onCleared();
        Log.i("MONVIEWMODEL","ViewModel DETRUIT");
    }
}
