package com.example.moviewatchers.viewmodel;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.moviewatchers.FavoritesFilmRoomDatabase;
import com.example.moviewatchers.dao.FilmDao;
import com.example.moviewatchers.models.FilmDetails;

import java.util.List;

public class FilmViewModel extends AndroidViewModel {
    private FilmDao filmDao;
    private FavoritesFilmRoomDatabase favoritesFilmDB;
    private LiveData<List<FilmDetails>> allFilms;

    public FilmViewModel(Application application) {
        super(application);
        favoritesFilmDB = FavoritesFilmRoomDatabase.getDatabase(application);
        filmDao = favoritesFilmDB.filmDao();
        allFilms = filmDao.getAllFavoritesFilms();
    }

    public void insert(FilmDetails film){
        new InsertAsyncTask(filmDao).execute(film);
    }

    public void delete(FilmDetails film) { new DeleteAsyncTask(filmDao).execute(film);}

    public LiveData<List<FilmDetails>> getAllFavoritesFilms(){
        return allFilms;
    }

    public LiveData<FilmDetails> getFilmById(int tmdb_id){
        return filmDao.getFilmById(tmdb_id);
    }


    private class OperationsAsyncTask extends AsyncTask<FilmDetails, Void, Void> {
        FilmDao asyncFilmDao;

        OperationsAsyncTask(FilmDao filmDao){
            this.asyncFilmDao = filmDao;
        }

        @Override
        protected Void doInBackground(FilmDetails... filmDetails) {
            return null;
        }
    }


    private class InsertAsyncTask extends OperationsAsyncTask {

        InsertAsyncTask(FilmDao filmDao) {super(filmDao);}

        @Override
        protected Void doInBackground(FilmDetails... filmDetails) {
            asyncFilmDao.insert(filmDetails[0]);
            return null;
        }
    }

    private class DeleteAsyncTask extends OperationsAsyncTask{

        DeleteAsyncTask(FilmDao filmDao) {super(filmDao);}

        @Override
        protected Void doInBackground(FilmDetails... filmDetails) {
            Log.i("DELETE UN FILM","JE SUIS DANS LE ONBACKGROUND DU DELETE");
            asyncFilmDao.delete(filmDetails[0]);
            return null;
        }
    }




    @Override
    protected void onCleared(){
        super.onCleared();
        Log.i("MONVIEWMODEL","ViewModel DETRUIT");
    }
}
