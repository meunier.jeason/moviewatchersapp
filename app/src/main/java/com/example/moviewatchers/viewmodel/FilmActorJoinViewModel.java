package com.example.moviewatchers.viewmodel;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.moviewatchers.FavoritesFilmRoomDatabase;
import com.example.moviewatchers.dao.FilmActorJoinDao;
import com.example.moviewatchers.models.ActorFilmCount;
import com.example.moviewatchers.models.FilmActorJoin;
import com.example.moviewatchers.models.FilmDetails;

import java.util.List;

public class FilmActorJoinViewModel extends AndroidViewModel {
    private FilmActorJoinDao filmActorJoinDao;
    private FavoritesFilmRoomDatabase favoritesFilmDB;
    private LiveData<List<FilmActorJoin>> allFilmActorJoin;

    public FilmActorJoinViewModel(Application application) {
        super(application);
        favoritesFilmDB = FavoritesFilmRoomDatabase.getDatabase(application);
        filmActorJoinDao = favoritesFilmDB.filmActorJoinDao();
        allFilmActorJoin = filmActorJoinDao.getAll();
    }

    public void insert(FilmActorJoin join){
        new InsertAsyncTask(filmActorJoinDao).execute(join);
    }

    public void delete(FilmActorJoin join) { new DeleteAsyncTask(filmActorJoinDao).execute(join);}

    LiveData<List<FilmActorJoin>> getAllFilmActorJoin(){
        return allFilmActorJoin;
    }

    public LiveData<List<ActorFilmCount>> getCountedByActor(){
        Log.d("MESLOGS", "je suis dans le viewmodel DE FILMJOIN");
        return filmActorJoinDao.getCountByActor();
    }

    //public List<FilmDetails> getActorForFilm(int film_id){
    //    return filmActorJoinDao.getActorForFilmId(film_id);
    //}

    public List<FilmDetails> getFilmForActor(int actor_id){
        return filmActorJoinDao.getFilmsForActorId(actor_id);
    }

    private class OperationsAsyncTask extends AsyncTask<FilmActorJoin, Void, Void> {
        FilmActorJoinDao joinDao;

        OperationsAsyncTask(FilmActorJoinDao joinDao){
            this.joinDao = joinDao;
        }

        @Override
        protected Void doInBackground(FilmActorJoin... filmActorJoins) {
            return null;
        }
    }


    private class InsertAsyncTask extends OperationsAsyncTask {

        InsertAsyncTask(FilmActorJoinDao filmActorJoinDao) {super(filmActorJoinDao);}

        @Override
        protected Void doInBackground(FilmActorJoin... filmActorJoins) {
            joinDao.insert(filmActorJoins[0]);
            return null;
        }
    }

    private class DeleteAsyncTask extends OperationsAsyncTask{

        DeleteAsyncTask(FilmActorJoinDao filmActorJoinDao) {super(filmActorJoinDao);}

        @Override
        protected Void doInBackground(FilmActorJoin... filmActorJoins) {
            Log.i("DELETE UN FILM","JE SUIS DANS LE ONBACKGROUND DU DELETE");
            joinDao.delete(filmActorJoins[0]);
            return null;
        }
    }




    @Override
    protected void onCleared(){
        super.onCleared();
        Log.i("MONVIEWMODEL","ViewModel DETRUIT");
    }
}
