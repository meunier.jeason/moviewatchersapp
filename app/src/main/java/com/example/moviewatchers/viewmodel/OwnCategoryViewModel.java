package com.example.moviewatchers.viewmodel;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.moviewatchers.FavoritesFilmRoomDatabase;
import com.example.moviewatchers.dao.ActorDao;
import com.example.moviewatchers.dao.OwnCategoryDao;
import com.example.moviewatchers.models.Actor;
import com.example.moviewatchers.models.OwnCategory;
import com.example.moviewatchers.models.OwnCategoryWithFilm;

import java.util.List;

public class OwnCategoryViewModel extends AndroidViewModel {
    private OwnCategoryDao ownCategoryDao;
    private FavoritesFilmRoomDatabase favoritesFilmDB;
    private LiveData<List<OwnCategory>> allCategories;

    public OwnCategoryViewModel(Application application) {
        super(application);
        favoritesFilmDB = FavoritesFilmRoomDatabase.getDatabase(application);
        ownCategoryDao = favoritesFilmDB.ownCategoryDao();
        allCategories = ownCategoryDao.getAllCategories();
    }

    public void insert(OwnCategory category){
        new InsertAsyncTask(ownCategoryDao).execute(category);
    }

    public void delete(OwnCategory category) { new DeleteAsyncTask(ownCategoryDao).execute(category);}

    public LiveData<List<OwnCategoryWithFilm>> getFilmsWithCategories(int category_id){
        return ownCategoryDao.getFilmsWithCategories(category_id);
    }

    public LiveData<List<OwnCategory>> getAllCategories(){
        return allCategories;
    }

    private class OperationsAsyncTask extends AsyncTask<OwnCategory, Void, Void> {
        OwnCategoryDao asyncDao;

        OperationsAsyncTask(OwnCategoryDao ownCategoryDao){
            this.asyncDao = ownCategoryDao;
        }

        @Override
        protected Void doInBackground(OwnCategory... categories) {
            return null;
        }
    }


    private class InsertAsyncTask extends OperationsAsyncTask {

        InsertAsyncTask(OwnCategoryDao ownCategoryDao) {super(ownCategoryDao);}

        @Override
        protected Void doInBackground(OwnCategory... categories) {
            asyncDao.insert(categories[0]);
            return null;
        }
    }


    private class DeleteAsyncTask extends  OperationsAsyncTask {

        DeleteAsyncTask(OwnCategoryDao ownCategoryDao) {super(ownCategoryDao);}

        @Override
        protected Void doInBackground(OwnCategory... categories) {
            asyncDao.delete(categories[0]);
            return null;
        }
    }


    @Override
    protected void onCleared(){
        super.onCleared();
        Log.i("MONVIEWMODEL","ViewModel DETRUIT");
    }
}
