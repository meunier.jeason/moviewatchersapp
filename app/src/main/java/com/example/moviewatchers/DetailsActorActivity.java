package com.example.moviewatchers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviewatchers.adapter.FilmAdapter;
import com.example.moviewatchers.dao.FilmActorJoinDao;
import com.example.moviewatchers.models.ActorFilmCount;
import com.example.moviewatchers.models.Film;
import com.example.moviewatchers.models.FilmDetails;
import com.example.moviewatchers.viewmodel.FilmActorJoinViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class DetailsActorActivity extends AppCompatActivity {
    private FilmActorJoinViewModel joinViewModel;
    private FilmActorJoinDao joinDao;
    private List<FilmDetails> films;
    private List<String> filmTitle = new ArrayList<>();
    private static Map<String, Bitmap> bitmaps = new HashMap<>();
    private List<Film> filmList = new ArrayList<>();
    private FilmAdapter filmAdapter = new FilmAdapter(filmList);
    private RecyclerView recyclerView;

    private int page=1;
    private int maxPage;
    private ActorFilmCount actor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actor);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);

        joinViewModel = new ViewModelProvider(this).get(FilmActorJoinViewModel.class);
        actor = getIntent().getExtras().getParcelable("actor");
        films = joinViewModel.getFilmForActor(actor.actor_id);

        for(FilmDetails film:films){
            filmTitle.add(film.getTitle());
        }
        //SET DATA
        ImageView actorProfile = findViewById(R.id.ivActorFav);
        TextView nbfilms = findViewById(R.id.filmCount);
        TextView actorName = findViewById(R.id.tvNameActor);
        ListView listFilm = findViewById(R.id.listfilm);

        actorName.setText(actor.actor_name);
        nbfilms.setText(""+actor.nbfilms);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, filmTitle);

        listFilm.setAdapter(arrayAdapter);
        String urlCheck;
        if(actor.actor_profile.matches("https://image.tmdb.org/t/p/w500null")){
            urlCheck = "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Unknown_person.jpg/542px-Unknown_person.jpg";
        }else{
            urlCheck = actor.actor_profile;
        }
        if (bitmaps.containsKey(urlCheck)){
            actorProfile.setImageBitmap(bitmaps.get(urlCheck));
        }
        else {
            new LoadImageTask(actorProfile).execute(urlCheck);
        }

        recyclerView = findViewById(R.id.recyclerviewPlayIn);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(filmAdapter);
        findPlayIn(recyclerView,1);

    }




    // TOOLBAR MENU
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Si on clique sur un icon de l'appbar close l'activity
        finish();

        return super.onOptionsItemSelected(item);
    }

    // Films acteurs
    public void findPlayIn(View view, int page){
        URL url = createURL("person/"+actor.actor_id+"/movie_credits","include_adult=false&include_video=false&page="+page);
        if(url != null){
            GetFilmTask getFilmTask = new GetFilmTask(this);
            getFilmTask.execute(url);
        } else {
            Toast.makeText(this, "Url Invalide", Toast.LENGTH_LONG).show();
        }
    }


    private URL createURL(String endpoint, String searchFilters) {
        String apiKey = getString(R.string.apikey);
        String baseUrl = getString(R.string.apiurl);

        try{
            String urlString = baseUrl + endpoint +"?api_key="+apiKey+"&language=fr-FR&"+searchFilters;
            Log.d("MesLogs",urlString);
            return new URL(urlString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private class GetFilmTask extends AsyncTask<URL,Void, JSONObject> {
        private Context context;

        public GetFilmTask(Context c){
            context = c;
        }


        @Override
        protected JSONObject doInBackground(URL... params) {
            HttpsURLConnection connection = null;
            try {
                connection = (HttpsURLConnection)params[0].openConnection();
                connection.setConnectTimeout(5000);
                int res = connection.getResponseCode();
                if (res == HttpsURLConnection.HTTP_OK){
                    StringBuilder builder = new StringBuilder();
                    try(BufferedReader reader = new BufferedReader(
                            new InputStreamReader(connection.getInputStream())
                    )){
                        String line;
                        while ((line = reader.readLine()) != null){
                            builder.append(line);
                        }
                    } catch (IOException e){
                        e.printStackTrace();
                    }
                    return new JSONObject(builder.toString());
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject film){
            if(film != null){
                convertJsonToArrayList(film);
                filmAdapter.notifyDataSetChanged();
            }

        }

        private void convertJsonToArrayList(JSONObject film) {
            try{
                Log.d("MESLOGS",film.toString());
                JSONArray list = film.getJSONArray("cast");
                Log.d("MesLogs", list.toString());
                for (int i=0; i<list.length();i++){
                    JSONObject filmInfo = list.getJSONObject(i);
                    Log.d("MesLogs",filmInfo.toString());
                    int id = filmInfo.getInt("id");
                    String title = filmInfo.getString("title");
                    String overview = filmInfo.getString("overview");
                    String posterPath = filmInfo.getString("poster_path");
                    filmList.add(new Film(
                            id,
                            title,
                            overview,
                            posterPath
                    ));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private class LoadImageTask extends AsyncTask<String,Void, Bitmap> {
        private ImageView imageView;

        public LoadImageTask(ImageView i){
            imageView = i;
        }

        @Override
        protected Bitmap doInBackground(String... params){
            Bitmap bitmap = null;
            HttpURLConnection connection = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection)url.openConnection();
                connection.setConnectTimeout(5000);
                int reponse = connection.getResponseCode();
                if (reponse == HttpURLConnection.HTTP_OK) {
                    try (InputStream inputStream = connection.getInputStream()) {
                        bitmap = BitmapFactory.decodeStream(inputStream);
                        bitmaps.put(params[0], bitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                        ;
                    }
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
            finally {
                connection.disconnect();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap){
            imageView.setImageBitmap(bitmap);
        }
    }
}
