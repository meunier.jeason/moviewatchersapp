package com.example.moviewatchers;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Switch;

import androidx.appcompat.widget.Toolbar;

import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private  DrawerLayout drawerLayout;
    public Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Drawer Layout
        drawerLayout = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.toolbar);
        // Switch des fragments
        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.Fragment_container, new HomeFragment());

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_draw_open, R.string.navigation_draw_close);

        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        if(savedInstanceState==null){
            getSupportFragmentManager().beginTransaction().replace(R.id.Fragment_container, new HomeFragment()).commit();
            toolbar.setTitle("Les films à découvrir");
            navigationView.setCheckedItem(R.id.home);
        }

    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){

            case R.id.home:
                toolbar.setTitle("Les films à découvrir");
                getSupportFragmentManager().beginTransaction().replace(R.id.Fragment_container, new HomeFragment()).commit();
                break;
            case R.id.favorites:
                toolbar.setTitle("Mes films favoris");
                getSupportFragmentManager().beginTransaction().replace(R.id.Fragment_container, new FavoritesFragment()).commit();
                break;
            case R.id.actors:
                toolbar.setTitle("Mes acteurs");
                getSupportFragmentManager().beginTransaction().replace(R.id.Fragment_container, new ActorsFragment()).commit();
                break;
            case R.id.searchByGenres:
                toolbar.setTitle("Recherche par genre");
                getSupportFragmentManager().beginTransaction().replace(R.id.Fragment_container, new SearchGenreFragment()).commit();
                break;
            case R.id.searchByYears:
                toolbar.setTitle("Recherche par année");
                getSupportFragmentManager().beginTransaction().replace(R.id.Fragment_container, new SearchYearFragment()).commit();
                break;
            case R.id.addOwnCategory:
                toolbar.setTitle("Ajouter une catégorie");
                getSupportFragmentManager().beginTransaction().replace(R.id.Fragment_container, new AddCategoryFragment()).commit();
                break;
            case R.id.manageCategories:
                toolbar.setTitle("Mes catégories");
                getSupportFragmentManager().beginTransaction().replace(R.id.Fragment_container, new ManageCategoryFragment()).commit();
                break;
            // Dans le cas d'une activity
            /*case R.id.Activity :
                Intent intent = new Intent(this, ActivityName.class);
                startActivity(intent);
                break;
            */
        }

        // Ferme le menu apres avoir changé d'activité ou de fragment
        drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }

    // Ferme le drawer
    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }else{
            super.onBackPressed();
        }

    }
}