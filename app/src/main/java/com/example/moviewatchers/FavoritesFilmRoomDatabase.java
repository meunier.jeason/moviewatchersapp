package com.example.moviewatchers;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.moviewatchers.dao.ActorDao;
import com.example.moviewatchers.dao.FavoritesFilmDao;
import com.example.moviewatchers.dao.FilmActorJoinDao;
import com.example.moviewatchers.dao.FilmDao;
import com.example.moviewatchers.dao.OwnCategoryDao;
import com.example.moviewatchers.models.Actor;
import com.example.moviewatchers.models.FavoritesFilm;
import com.example.moviewatchers.models.FilmActorJoin;
import com.example.moviewatchers.models.FilmDetails;
import com.example.moviewatchers.models.OwnCategory;
import com.example.moviewatchers.models.OwnCategoryWithFilm;

@Database(entities = {FavoritesFilm.class,
        FilmDetails.class,
        Actor.class,
        FilmActorJoin.class,
        OwnCategory.class
    }, version = 2)
public abstract class FavoritesFilmRoomDatabase extends RoomDatabase {

    public abstract FavoritesFilmDao favoritesFilmDao();
    public abstract FilmDao filmDao();
    public abstract ActorDao actorDao();
    public abstract FilmActorJoinDao filmActorJoinDao();
    public abstract OwnCategoryDao ownCategoryDao();
    private static volatile FavoritesFilmRoomDatabase favoriteFilmRoomInstance;
    public static FavoritesFilmRoomDatabase getDatabase(final Context context){
        if(favoriteFilmRoomInstance == null){
            synchronized (FavoritesFilmRoomDatabase.class){
                if(favoriteFilmRoomInstance == null){
                    favoriteFilmRoomInstance = Room.databaseBuilder(context.getApplicationContext(),
                            FavoritesFilmRoomDatabase.class, "favorites_database").allowMainThreadQueries().fallbackToDestructiveMigration().build();
                }
            }
        }
        return favoriteFilmRoomInstance;
    }
}

