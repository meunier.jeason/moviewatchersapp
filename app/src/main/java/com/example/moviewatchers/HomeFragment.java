package com.example.moviewatchers;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviewatchers.adapter.FilmAdapter;
import com.example.moviewatchers.models.Film;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;


public class HomeFragment extends Fragment {
    private List<Film> filmList = new ArrayList<>();
    private FilmAdapter filmAdapter = new FilmAdapter(filmList);
    private LinearLayoutManager linearLayoutManager;
    private int page=1;
    private int maxPage;
    //Search
    private FloatingActionButton fabSearch;
    private boolean searchStatus = false;
    private EditText searchEdt;
    private String searchParam;
    private RecyclerView recyclerView;
    private TextView tvError;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return inflater.inflate(R.layout.homefragment,container,false);


        View view = inflater.inflate(R.layout.homefragment, container, false);
        recyclerView = view.findViewById(R.id.recyclerviewFilms);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(filmAdapter);
        findDiscover(recyclerView,1);
        searchEdt = view.findViewById(R.id.edtSearch);
        tvError = view.findViewById(R.id.tvError);

        // Scroll continu avec appel a l'api
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == filmList.size()-1){
                    if (maxPage == 0 || page < maxPage){
                        page = page + 1;
                        if(filmAdapter.getItemCount() == 0){
                            tvError.setText("L'api n'a retourné aucun film");
                        }else{
                            tvError.setText("");
                        }
                        if(searchStatus == false){
                            findDiscover(recyclerView,page);
                        }else{
                            search(recyclerView,searchParam,page);
                        }

                    }

                }
            }
        });


        fabSearch = view.findViewById(R.id.fabSearch);
        fabSearch.setOnClickListener(v -> {
            Log.d("MESLOGS",""+searchStatus);
            searchParam = searchEdt.getText().toString();
            if (searchParam.matches("")) {
                searchClear();
                searchStatus = false;
            } else {
                searchStatus = !searchStatus;
                if(searchStatus == true) {
                    filmList.clear();
                    linearLayoutManager.scrollToPositionWithOffset(0, 0);
                    fabSearch.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_cancel));
                    Log.d("MESLOGS", "" + searchParam);
                    search(recyclerView,searchParam,1);
                    MainActivity activity = (MainActivity) getActivity();
                    activity.toolbar.setTitle("Recherche pour: "+searchParam);
                } else{
                    searchClear();
                }
            }

        });

        return view;

    }

    public void searchClear(){
        filmList.clear();
        linearLayoutManager.scrollToPositionWithOffset(0, 0);
        fabSearch.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_search));
        searchEdt.setText("");
        page = 1;
        searchStatus = false;
        findDiscover(recyclerView,page);

        MainActivity activity = (MainActivity) getActivity();
        activity.toolbar.setTitle("Les films à découvrir");
    }


    public void findDiscover(View view, int page){
        URL url = createURL("discover/movie","sort_by=popularity.desc&include_adult=false&include_video=false&page="+page);
        if(url != null){
            GetFilmTask getFilmTask = new GetFilmTask(getActivity());
            getFilmTask.execute(url);
        } else {
            Toast.makeText(getActivity(), "Url Invalide", Toast.LENGTH_LONG).show();
        }
    }

    public void search(View view, String searchParam, int page){
        URL url = createURL("search/movie","query="+searchParam+"&include_adult=false&page="+page);
        if(url != null){
            GetFilmTask getFilmTask = new GetFilmTask(getActivity());
            getFilmTask.execute(url);
        } else {
            Toast.makeText(getActivity(), "Url Invalide", Toast.LENGTH_LONG).show();
        }
    }

    private URL createURL(String endpoint, String searchFilters) {
        String apiKey = getString(R.string.apikey);
        String baseUrl = getString(R.string.apiurl);

        try{
            String urlString = baseUrl + endpoint +"?api_key="+apiKey+"&language=fr-FR&"+searchFilters;
            Log.d("MesLogs",urlString);
            return new URL(urlString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private class GetFilmTask extends AsyncTask<URL,Void, JSONObject> {
        private Context context;

        public GetFilmTask(Context c){
            context = c;
        }


        @Override
        protected JSONObject doInBackground(URL... params) {
            HttpsURLConnection connection = null;
            try {
                connection = (HttpsURLConnection)params[0].openConnection();
                connection.setConnectTimeout(5000);
                int res = connection.getResponseCode();
                if (res == HttpsURLConnection.HTTP_OK){
                    StringBuilder builder = new StringBuilder();
                    try(BufferedReader reader = new BufferedReader(
                            new InputStreamReader(connection.getInputStream())
                    )){
                        String line;
                        while ((line = reader.readLine()) != null){
                            builder.append(line);
                        }
                    } catch (IOException e){
                        e.printStackTrace();
                    }
                    return new JSONObject(builder.toString());
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject film){
            if(film != null){
                convertJsonToArrayList(film);
                filmAdapter.notifyDataSetChanged();
            }

        }

        private void convertJsonToArrayList(JSONObject film) {
            try{
                JSONArray list = film.getJSONArray("results");
                //Log.d("MesLogs", list.toString());
                for (int i=0; i<list.length();i++){
                    JSONObject filmInfo = list.getJSONObject(i);
                    //Log.d("MesLogs",filmInfo.toString());
                    int id = filmInfo.getInt("id");
                    String title = filmInfo.getString("title");
                    String overview = filmInfo.getString("overview");
                    String posterPath = filmInfo.getString("poster_path");
                    filmList.add(new Film(
                            id,
                            title,
                            overview,
                            posterPath
                    ));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }


}
