package com.example.moviewatchers.adapter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviewatchers.DetailsFilmsActivity;
import com.example.moviewatchers.R;
import com.example.moviewatchers.models.Film;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FilmAdapter extends RecyclerView.Adapter<FilmAdapter.ViewHolder> {
    private static List<Film> filmList;
    private ViewHolder holder;
    private static Map<String, Bitmap> bitmaps = new HashMap<>();

    public FilmAdapter(List<Film> filmList) {
        this.filmList = filmList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.film_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Film film = filmList.get(position);
        holder.display(film);
    }

    @Override
    public int getItemCount() {
        return filmList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView titre;
        private final TextView overview;
        private final ImageView poster;
        private final CardView cv;

        public ViewHolder(View view) {
            super(view);
            titre = view.findViewById(R.id.tvTitle);
            overview = view.findViewById(R.id.tvOverview);
            poster = view.findViewById(R.id.ivPoster);
            cv = view.findViewById(R.id.cardview);
            cv.setOnClickListener(v -> {
                Intent intent = new Intent(v.getContext(), DetailsFilmsActivity.class);
                Log.d("MesLogs: ", "" + filmList.get(getAdapterPosition()));
                int filmId = filmList.get(getAdapterPosition()).getId();
                intent.putExtra("movie_id", filmId);
                v.getContext().startActivity(intent);
            });
        }

        public void display(Film film) {
            String titleCheck;
            String overviewCheck;
            String urlCheck;
            if(film.getTitle().matches(""))
                titleCheck = "Non renseigné";
            else
                titleCheck = film.getTitle();
            if(film.getOverview().matches(""))
                overviewCheck = "Non renseigné";
            else
                overviewCheck = film.getOverview();
            if(film.getImageURL().matches("https://image.tmdb.org/t/p/w500null"))
                urlCheck = "https://movie.boom-ker.live/img/notfound.png";
            else
                urlCheck = film.getImageURL();
            titre.setText(titleCheck);
            overview.setText(overviewCheck);
            if (bitmaps.containsKey(urlCheck)) {
                poster.setImageBitmap(bitmaps.get(urlCheck));
            } else {
                new LoadImageTask(poster).execute(urlCheck);
            }
        }

        private class LoadImageTask extends AsyncTask<String, Void, Bitmap> {
            private ImageView imageView;

            public LoadImageTask(ImageView i) {
                imageView = i;
            }

            @Override
            protected Bitmap doInBackground(String... params) {
                Bitmap bitmap = null;
                HttpURLConnection connection = null;

                try {
                    URL url = new URL(params[0]);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setConnectTimeout(5000);
                    int reponse = connection.getResponseCode();
                    if (reponse == HttpURLConnection.HTTP_OK) {
                        try (InputStream inputStream = connection.getInputStream()) {
                            bitmap = BitmapFactory.decodeStream(inputStream);
                            bitmaps.put(params[0], bitmap);
                        } catch (Exception e) {
                            e.printStackTrace();
                            ;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    connection.disconnect();
                }
                return bitmap;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                imageView.setImageBitmap(bitmap);
            }
        }
    }
}
