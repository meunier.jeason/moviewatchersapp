package com.example.moviewatchers.adapter;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviewatchers.FavoritesByCategoryActivity;
import com.example.moviewatchers.R;
import com.example.moviewatchers.models.OwnCategory;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class OwnCategoryAdapter extends RecyclerView.Adapter<OwnCategoryAdapter.ViewHolder>{
    private LayoutInflater inflater;
    private Context context;
    private OnDeleteClickListener onDeleteClickListener;
    private List<OwnCategory> categories;


    public OwnCategoryAdapter(Context context, OnDeleteClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.onDeleteClickListener = listener;
        this.context = context;
    }

    public interface OnDeleteClickListener{
        void  OnDeleteClickListener(OwnCategory category);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.category_item,parent,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(categories != null){
            Log.d("DANS LE BIND VIEW HOLDER",""+categories);
            OwnCategory current = categories.get(position);
            holder.display(current);
        }else{
            Log.d("DANS LE BIND VIEW HOLDER","SUCE MON CHIBRE");
            holder.tvCatName.setText("Aucune catégorie");
        }

    }

    @Override
    public int getItemCount() {
       if(categories != null){
           return categories.size();
       }else{
           return 0;
       }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvCatName;
        private final TextView tvCatDesc;
        private final FloatingActionButton fabCatDel;
        private final CardView cvCat;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCatName = itemView.findViewById(R.id.tvNameCat);
            tvCatDesc = itemView.findViewById(R.id.tvDescCat);
            fabCatDel = itemView.findViewById(R.id.fabCatDel);
            cvCat = itemView.findViewById(R.id.cardviewCat);

        }

        public void display(OwnCategory current){
            Log.d("DANS LA FONCTION DISPLAY", ""+current);
            tvCatName.setText(current.getName());
            tvCatDesc.setText(current.getDescription());
            fabCatDel.setOnClickListener(v -> {
                Log.d("MESLOGS DE SUPPRESSION", "COUCOU");
                if(onDeleteClickListener != null){
                    Log.d("MESLOGS DE SUPPRESSION", "JE SUIS DANS LE ON CLICK DEL");
                    onDeleteClickListener.OnDeleteClickListener(current);
                }
            });
            cvCat.setOnClickListener(v -> {
                Intent intent = new Intent(v.getContext(), FavoritesByCategoryActivity.class);
                int categoryId = current.getId();
                intent.putExtra("category_id", categoryId);
                intent.putExtra("category_name",current.getName());
                v.getContext().startActivity(intent);
            });
        }
    }

    public void setCategories(List<OwnCategory> categories){
        Log.d("DANS LE SET CATEGORIES",""+categories);
        this.categories = categories;
        notifyDataSetChanged();
    }
}
