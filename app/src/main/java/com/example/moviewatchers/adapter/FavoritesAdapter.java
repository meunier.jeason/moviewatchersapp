package com.example.moviewatchers.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviewatchers.DetailsFilmsActivity;
import com.example.moviewatchers.R;
import com.example.moviewatchers.models.FilmDetails;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FavoritesAdapter extends RecyclerView.Adapter<FavoritesAdapter.FavoritesViewHolder> implements Filterable {
    private List<FilmDetails> filmsListFull;


    public interface OnDeleteClickListener{
        void  OnDeleteClickListener(FilmDetails film);
    }


    private static Map<String, Bitmap> bitmaps = new HashMap<>();
    class FavoritesViewHolder extends RecyclerView.ViewHolder{
        private final TextView tvTitle;
        private final TextView tvOverview;
        private final ImageView ivPoster;
        private final FloatingActionButton delBtn;
        private final CardView cardView;

        private FavoritesViewHolder(View itemView){
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitleFav);
            tvOverview = itemView.findViewById(R.id.tvOverviewFav);
            ivPoster = itemView.findViewById(R.id.ivPosterFav);
            delBtn = itemView.findViewById(R.id.fabDelete);
            cardView = itemView.findViewById(R.id.cardviewFav);
        }

        public void display(FilmDetails current) {
            tvTitle.setText(current.getTitle());
            tvOverview.setText(current.getOverview());
            String urlChecker;
            if(current.getPosterURL().matches("https://image.tmdb.org/t/p/w500null")){
                urlChecker = "https://movie.boom-ker.live/img/notfound.png";
            }else{
                urlChecker = current.getPosterURL();
            }
            if (bitmaps.containsKey(urlChecker)) {
                ivPoster.setImageBitmap(bitmaps.get(urlChecker));
            } else {
                new LoadImageTask(ivPoster).execute(urlChecker);
            }
            delBtn.setOnClickListener(v -> {
                if(onDeleteClickListener != null){
                    onDeleteClickListener.OnDeleteClickListener(current);
                }
            });
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(v.getContext(), "J'ai cliqué sur " + titre.getText(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(v.getContext(), DetailsFilmsActivity.class);
                    int filmId =current.getId();
                    intent.putExtra("movie_id", filmId);
                    v.getContext().startActivity(intent);
                }
            });

        }

        private class LoadImageTask extends AsyncTask<String, Void, Bitmap> {
            private ImageView imageView;

            public LoadImageTask(ImageView i) {
                Log.d("MON IMAGE",""+i);
                imageView = i;
            }

            @Override
            protected Bitmap doInBackground(String... params) {
                Bitmap bitmap = null;
                HttpURLConnection connection = null;

                try {
                    Log.d("IMAGE", "JE SUIS LA");
                    URL url = new URL(params[0]);
                    Log.d("IMAGE", url.toString());
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setConnectTimeout(5000);
                    int reponse = connection.getResponseCode();
                    if (reponse == HttpURLConnection.HTTP_OK) {
                        try (InputStream inputStream = connection.getInputStream()) {
                            bitmap = BitmapFactory.decodeStream(inputStream);
                            bitmaps.put(params[0], bitmap);

                        } catch (Exception e) {
                            e.printStackTrace();
                            ;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    connection.disconnect();
                }
                return bitmap;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                imageView.setImageBitmap(bitmap);
            }
        }

    }

    private final LayoutInflater inflater;
    private List<FilmDetails> films;
    private OnDeleteClickListener onDeleteClickListener;
    public FavoritesAdapter(Context context, OnDeleteClickListener listener){
        inflater = LayoutInflater.from(context);
        this.onDeleteClickListener = listener;
    }




    @NonNull
    @Override
    public FavoritesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.favorites_item,parent, false);
        return new FavoritesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoritesAdapter.FavoritesViewHolder holder, int position) {
        if(films != null){
            FilmDetails current =  films.get(position);
            holder.display(current);
        }
        else{
            holder.tvTitle.setText("No words");
        }
    }

    public void setFavFilms(List<FilmDetails> films){
        Log.d("MESLOGS", "COUCOU"+films);
        this.films = films;
        this.filmsListFull = new ArrayList<>(films);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(films != null){
            return films.size();
        }else{
            return 0;
        }
    }

    @Override
    public Filter getFilter() {
        return filmFilter;
    }
    private Filter filmFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<FilmDetails> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(filmsListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (FilmDetails item : filmsListFull) {
                    if (item.getTitle().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            films.clear();
            films.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };


}
