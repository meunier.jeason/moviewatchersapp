package com.example.moviewatchers.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.moviewatchers.R;
import com.example.moviewatchers.models.Actor;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActorsAdapter extends RecyclerView.Adapter<ActorsAdapter.ViewHolder> {
    private int actorId;
    private List<Actor> actors;
    private static Map<String, Bitmap> bitmaps = new HashMap<>();
    private final LayoutInflater inflater;
    public ActorsAdapter(Context context) {
        //this.actors = actors;
        inflater = LayoutInflater.from(context);
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView name;
        private final ImageView profile ;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View
            name = view.findViewById(R.id.tvActor);
            profile = view.findViewById(R.id.ivActor);
        }

        public void display(Actor actor){
            String urlCheck;
            name.setText(actor.getName());
            if(actor.getProfile_url().matches("https://image.tmdb.org/t/p/w500null"))
                urlCheck = "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Unknown_person.jpg/542px-Unknown_person.jpg";
            else
                urlCheck = actor.getProfile_url();

            if (bitmaps.containsKey(urlCheck)){
                profile.setImageBitmap(bitmaps.get(urlCheck));
            }
            else {
                new ViewHolder.LoadImageTask(profile).execute(urlCheck);
            }
        }


        private class LoadImageTask extends AsyncTask<String,Void,Bitmap> {
            private ImageView imageView;

            public LoadImageTask(ImageView i){
                imageView = i;
            }

            @Override
            protected Bitmap doInBackground(String... params){
                Bitmap bitmap = null;
                HttpURLConnection connection = null;

                try {
                    URL url = new URL(params[0]);
                    connection = (HttpURLConnection)url.openConnection();
                    connection.setConnectTimeout(5000);
                    int reponse = connection.getResponseCode();
                    if (reponse == HttpURLConnection.HTTP_OK) {
                        try (InputStream inputStream = connection.getInputStream()) {
                            bitmap = BitmapFactory.decodeStream(inputStream);
                            bitmaps.put(params[0], bitmap);
                        } catch (Exception e) {

                            e.printStackTrace();
                            ;
                        }
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                finally {
                    connection.disconnect();
                }
                return bitmap;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap){
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    public void setActors(List<Actor> actors){
        this.actors = actors;
        notifyDataSetChanged();
    }
    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.actor_item, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Actor actor = actors.get(position);
        viewHolder.display(actor);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return actors.size();
    }
}