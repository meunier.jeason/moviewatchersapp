package com.example.moviewatchers.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviewatchers.DetailsActorActivity;
import com.example.moviewatchers.R;
import com.example.moviewatchers.models.ActorFilmCount;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FilmActorJoinAdapter extends RecyclerView.Adapter<FilmActorJoinAdapter.ViewHolder> implements Filterable {

    private static Map<String, Bitmap> bitmaps = new HashMap<>();
    private final LayoutInflater inflater;
    private Context context;
    private List<ActorFilmCount> actors;
    private List<ActorFilmCount> actorListFull;

    public FilmActorJoinAdapter(Context context){
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public Filter getFilter() {
        return actorFilter;
    }

    private Filter actorFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<ActorFilmCount> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(actorListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (ActorFilmCount item : actorListFull) {
                    if (item.actor_name.toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            actors.clear();
            actors.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView tvActorName;
        private final ImageView ivProfile;
        private final TextView tvFilmNumber;
        private final LinearLayout ll;
        private CardView cv;

        private ViewHolder(View itemView){
            super(itemView);
            tvActorName = itemView.findViewById(R.id.tvNameActor);
            ivProfile = itemView.findViewById(R.id.ivActorFav);
            tvFilmNumber = itemView.findViewById(R.id.filmCount);
            ll = itemView.findViewById(R.id.llActor);
            cv = itemView.findViewById(R.id.cardviewActor);

        }

        public void display(ActorFilmCount current, int position) {
            RelativeLayout.LayoutParams ivlp = (RelativeLayout.LayoutParams) ivProfile.getLayoutParams();
            RelativeLayout.LayoutParams lllp = (RelativeLayout.LayoutParams) ll.getLayoutParams();
            if(position%2 == 0 || position == 0){
                ivlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                ivlp.removeRule(RelativeLayout.ALIGN_PARENT_LEFT);
                lllp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                lllp.removeRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            } else {
                ivlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                ivlp.removeRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                lllp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                lllp.removeRule(RelativeLayout.ALIGN_PARENT_LEFT);
            }
            ivProfile.setLayoutParams(ivlp);
            Log.d("MESLOGS", ""+current.nbfilms);
            tvActorName.setText(current.actor_name);
            tvFilmNumber.setText(""+current.nbfilms);
            String urlChecker;
            if(current.actor_profile.matches("https://image.tmdb.org/t/p/w500null")){
                urlChecker = "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Unknown_person.jpg/542px-Unknown_person.jpg";
            }else{
                urlChecker = current.actor_profile;
            }
            if (bitmaps.containsKey(urlChecker)) {
                ivProfile.setImageBitmap(bitmaps.get(urlChecker));
            } else {
                new LoadImageTask(ivProfile).execute(urlChecker);
            }
            cv.setOnClickListener(v -> {
                Intent intent = new Intent(v.getContext(), DetailsActorActivity.class);
                intent.putExtra("actor", (Parcelable) current);
                v.getContext().startActivity(intent);
            });
        }

        private class LoadImageTask extends AsyncTask<String, Void, Bitmap> {
            private ImageView imageView;

            public LoadImageTask(ImageView i) {
                Log.d("MON IMAGE",""+i);
                imageView = i;
            }

            @Override
            protected Bitmap doInBackground(String... params) {
                Bitmap bitmap = null;
                HttpURLConnection connection = null;

                try {
                    Log.d("IMAGE", "JE SUIS LA");
                    URL url = new URL(params[0]);
                    Log.d("IMAGE", url.toString());
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setConnectTimeout(5000);
                    int reponse = connection.getResponseCode();
                    if (reponse == HttpURLConnection.HTTP_OK) {
                        try (InputStream inputStream = connection.getInputStream()) {
                            bitmap = BitmapFactory.decodeStream(inputStream);
                            bitmaps.put(params[0], bitmap);

                        } catch (Exception e) {
                            e.printStackTrace();
                            ;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    connection.disconnect();
                }
                return bitmap;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                imageView.setImageBitmap(bitmap);
            }
        }

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.actorsfav_item,parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(actors != null){

            ActorFilmCount current =  actors.get(position);
            holder.display(current, position);
        }
        else{
            holder.tvActorName.setText("No words");
        }
    }

    public void setActors(List<ActorFilmCount> actors){
        this.actors = actors;
        this.actorListFull = new ArrayList<>(actors);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(actors != null){
            return actors.size();
        }else{
            return 0;
        }
    }

}
