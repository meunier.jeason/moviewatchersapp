package com.example.moviewatchers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviewatchers.adapter.FilmActorJoinAdapter;
import com.example.moviewatchers.viewmodel.FilmActorJoinViewModel;


public class ActorsFragment extends Fragment {
    //private List<FilmDetails> favoritesFilms = new ArrayList<>();
    private FilmActorJoinAdapter joinAdapter;
    private FilmActorJoinViewModel joinViewModel;
    private SearchView searchView;
    private TextView tvError;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.actorsfragment, container, false);
        joinViewModel = new ViewModelProvider(this).get(FilmActorJoinViewModel.class);
        joinAdapter = new FilmActorJoinAdapter(getContext());
        RecyclerView recyclerView = view.findViewById(R.id.recyclerviewActorsFav);
        searchView = view.findViewById(R.id.searchActor);
        tvError = view.findViewById(R.id.tvError);

        recyclerView.setAdapter(joinAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        joinViewModel.getCountedByActor().observe(getViewLifecycleOwner(), actors -> {
            joinAdapter.setActors(actors);
            if (joinAdapter.getItemCount() == 0) {
                tvError.setText("Aucun acteur dans vos films favoris");
            }else{
                tvError.setText("");
            }
        });


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                joinAdapter.getFilter().filter(newText);
                return false;
            }
        });


        return view;

        //return inflater.inflate(R.layout.favoritesfragment,container,false);
    }

}
