package com.example.moviewatchers;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import androidx.lifecycle.LiveData;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moviewatchers.adapter.ActorsAdapter;
import com.example.moviewatchers.models.Actor;
import com.example.moviewatchers.models.FavoritesFilm;
import com.example.moviewatchers.models.FilmActorJoin;
import com.example.moviewatchers.models.FilmDetails;
import com.example.moviewatchers.models.OwnCategory;
import com.example.moviewatchers.viewmodel.ActorViewModel;
import com.example.moviewatchers.viewmodel.FavoritesViewModel;
import com.example.moviewatchers.viewmodel.FilmActorJoinViewModel;
import com.example.moviewatchers.viewmodel.FilmViewModel;
import com.example.moviewatchers.viewmodel.OwnCategoryViewModel;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.ui.PlayerUiController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import javax.net.ssl.HttpsURLConnection;

public class DetailsFilmsActivity extends AppCompatActivity {
    private int filmId;
    private List<FilmDetails> filmDetails = new ArrayList<>();
    private List<Actor> actors = new ArrayList<>();
    private ActorsAdapter actorsAdapter;
    private static Map<String, Bitmap> bitmaps = new HashMap<>();
    private LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
    private Menu menu;
    private CollapsingToolbarLayout toolBarLayout;
    private FavoritesViewModel favoritesViewModel;
    private FilmViewModel filmViewModel;
    private FavoritesFilm favoritesFilm;
    private FilmActorJoinViewModel filmActorJoinViewModel;
    private ActorViewModel actorViewModel;
    private FloatingActionButton fab;
    private LiveData<List<OwnCategory>> liveDataCategories;
    private List<OwnCategory> categories;
    private List<String> categoriesName = new ArrayList<>();
    private int category_id = -1;
    private String trailer_ytId = "";
    private YouTubePlayerView youTubePlayerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_scrolling);

        // TOOLBAR COLLAPSE

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        OwnCategoryViewModel ownCategoryViewModel = new ViewModelProvider(this).get(OwnCategoryViewModel.class);
        liveDataCategories = ownCategoryViewModel.getAllCategories();
        liveDataCategories.observe(this, ownCategories -> {
            categories = ownCategories;
        });
        favoritesViewModel = new ViewModelProvider(this).get(FavoritesViewModel.class);
        filmViewModel = new ViewModelProvider(this).get(FilmViewModel.class);
        filmActorJoinViewModel = new ViewModelProvider(this).get(FilmActorJoinViewModel.class);
        actorViewModel = new ViewModelProvider(this).get(ActorViewModel.class);
        toolBarLayout = findViewById(R.id.toolbar_layout);

        fab = findViewById(R.id.fab);


        filmId = getIntent().getExtras().getInt("movie_id");
        FilmDetail();
        LiveData<FavoritesFilm> isFavorite = favoritesViewModel.getByTmdbId(filmId);
        isFavorite.observe(this, favoritesFilm -> {
            if(favoritesFilm != null){
                fab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_favorite));
                fab.setOnClickListener(view -> removeFromFavorite(view, isFavorite));
            } else {
                fab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_nofavorite));
                //fab.setOnClickListener(view -> addToFavorite(view));
                fab.setOnClickListener(view -> loadModal(view));
            }
        });


        RecyclerView rvActors = findViewById(R.id.recyclerviewActors);
        rvActors.setLayoutManager(linearLayoutManager);
        actorsAdapter = new ActorsAdapter(this);
        actorsAdapter.setActors(actors);
        rvActors.setAdapter(actorsAdapter);



    }

    private void loadModal(View view) {
        categoriesName.clear();
        for(OwnCategory category:categories){
            categoriesName.add(category.getName());
        }

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Choisir une catégorie");

        // add a radio button list
        builder.setSingleChoiceItems(categoriesName.toArray(new String[categoriesName.size()]), -1, (dialog, which) -> {
            category_id = categories.get(which).getId();

        });
        if(categories.size() < 1){
            builder.setMessage("Il faut créer une catégorie depuis le menu afin d'ajouter des films à vos favoris");
        }else {
            // add OK and Cancel buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    if(category_id == -1) {
                        Snackbar.make(view, "Merci de sélectionner une catégorie!", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }else{
                        addToFavorite(view);

                    }

                }
            });
        }
        builder.setNegativeButton("Annuler", null);

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void addToFavorite(View view) {
        favoritesViewModel.insert(favoritesFilm);
        filmDetails.get(0).setOwn_category_id(category_id);
        filmViewModel.insert(filmDetails.get(0));
        for(Actor actor:actors){
            actorViewModel.insert(actor);
            filmActorJoinViewModel.insert(new FilmActorJoin(filmId,actor.getId()));
        }
        fab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_favorite));
        Snackbar.make(view, "Ajouté aux favoris", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    private void removeFromFavorite(View view, LiveData<FavoritesFilm> film) {
        favoritesViewModel.delete(film.getValue());
        filmViewModel.delete(filmDetails.get(0));
        //filmViewModel.delete(filmDetails.get(0));
        category_id = -1;
        fab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_nofavorite));
        Snackbar.make(view, "Supprimé des favoris", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    // TOOLBAR MENU
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Si on clique sur un icon de l'appbar close l'activity
        finish();

        return super.onOptionsItemSelected(item);
    }





    public void FilmDetail(){
        URL url = createURL("movie/"+filmId,"&append_to_response=credits,videos");
        if(url != null){
            GetDetailsTask getFilmTask = new GetDetailsTask(this);
            getFilmTask.execute(url);
        } else {
            Toast.makeText(this, "Url Invalide", Toast.LENGTH_LONG).show();
        }
    }

    private URL createURL(String endpoint, String searchFilters) {
        String apiKey = getString(R.string.apikey);
        String baseUrl = getString(R.string.apiurl);

        try{
            String urlString = baseUrl + endpoint +"?api_key="+apiKey+"&language=fr-FR&"+searchFilters;
            return new URL(urlString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }


    private class GetDetailsTask extends AsyncTask<URL,Void, JSONObject> {
        private Context context;

        public GetDetailsTask(Context c){
            context = c;
        }


        @Override
        protected JSONObject doInBackground(URL... params) {
            HttpsURLConnection connection = null;
            try {
                connection = (HttpsURLConnection)params[0].openConnection();
                connection.setConnectTimeout(5000);
                int res = connection.getResponseCode();
                if (res == HttpsURLConnection.HTTP_OK){
                    StringBuilder builder = new StringBuilder();
                    try(BufferedReader reader = new BufferedReader(
                            new InputStreamReader(connection.getInputStream())
                    )){
                        String line;
                        while ((line = reader.readLine()) != null){
                            builder.append(line);
                        }
                    } catch (IOException e){
                        e.printStackTrace();
                    }
                    return new JSONObject(builder.toString());
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject film){
            ImageView backdrop = findViewById(R.id.ivBackdrop);
            TextView title = findViewById(R.id.tvTitleFull);
            TextView date = findViewById(R.id.tvReleaseDate);
            TextView genres = findViewById(R.id.tvGenres);
            TextView overview = findViewById(R.id.tvDetailsOverview);
            TextView noTrailer = findViewById(R.id.tvNoTrailer);
            RelativeLayout rlTrailer = findViewById(R.id.rlTrailer);

            if(film != null) {
                convertJsonToArrayList(film);
                if (bitmaps.containsKey(filmDetails.get(0).getBackdropURL())) {
                    backdrop.setImageBitmap(bitmaps.get(filmDetails.get(0).getBackdropURL()));
                } else {
                    new LoadImageTask(backdrop).execute(filmDetails.get(0).getBackdropURL());
                }
                toolBarLayout.setTitle(filmDetails.get(0).getTitle());
                // Creation du favoris
                favoritesFilm = new FavoritesFilm(filmDetails.get(0).getTitle(), filmId);

                title.setText(filmDetails.get(0).getTitle());
                date.setText(filmDetails.get(0).getDate());
                genres.setText(filmDetails.get(0).getGenres());
                overview.setText(filmDetails.get(0).getOverview());
                actorsAdapter.notifyDataSetChanged();
                if (trailer_ytId.matches("")) {
                    noTrailer.setText("Il n'y a pas de trailer disponible");
                } else {
                    youTubePlayerView = new YouTubePlayerView(context);
                    rlTrailer.addView(youTubePlayerView);
                    getLifecycle().addObserver(youTubePlayerView);
                    youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
                        @Override
                        public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                            String videoId = trailer_ytId;
                            PlayerUiController test = youTubePlayerView.getPlayerUiController();
                            test.showFullscreenButton(false);
                            youTubePlayer.cueVideo(videoId, 0);
                        }
                    });
                }
            }
        }

        private void convertJsonToArrayList(JSONObject film) {
            try{
                String title = film.getString("title");
                String overview = film.getString("overview");
                String date = film.getString("release_date");
                //On recupere le nom des genres et on en fait une string
                JSONArray genresArray = film.getJSONArray("genres");
                StringJoiner joiner = new StringJoiner(", ");
                for(int i=0; i < genresArray.length();i++){
                    String genreName = genresArray.getJSONObject(i).getString("name");
                    joiner.add(genreName);
                }
                String genres = joiner.toString();

                String backdrop_path = film.getString("backdrop_path");
                String posterPath = film.getString("poster_path");
                JSONArray castingArray = film.getJSONObject("credits").getJSONArray("cast");
                actors.clear();
                for (int i=0; i<10;i++){
                    if(castingArray.length() > i){

                        JSONObject actor = castingArray.getJSONObject(i);
                        int id = actor.getInt("id");
                        String name = actor.getString("name");
                        String profile_url = actor.getString("profile_path");

                        actors.add(new Actor(
                                id,
                                filmId,
                                name,
                                profile_url
                        ));
                    }
                }


                // GET TRAILER ID
                JSONArray videosArray = film.getJSONObject("videos").getJSONArray("results");
                for(int i=0; i < videosArray.length(); i++){
                    JSONObject video = videosArray.getJSONObject(i);
                    if(trailer_ytId.matches("")) {
                        if (video.getString("type").matches("Trailer") && video.getString("site").matches("YouTube")) {
                            trailer_ytId = video.getString("key");
                            Log.d("MON ID", "id= = " + trailer_ytId);
                            //return;
                        }
                    }

                }



                if(overview.matches("")){
                    overview = "Non renseigné";
                }
                if(genres.matches("")){
                    genres = "Non renseigné";
                }
                if(date.matches("")){
                    date = "Non renseigné";
                }
                if(posterPath.matches("")) {
                    posterPath = "https://movie.boom-ker.live/img/notfound.png";
                }
                int own_category = 1;



                filmDetails.add(new FilmDetails(
                        filmId,
                        own_category,
                        title,
                        overview,
                        date,
                        genres,
                        backdrop_path,
                        posterPath
                ));


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private class LoadImageTask extends AsyncTask<String,Void, Bitmap> {
        private ImageView imageView;

        public LoadImageTask(ImageView i){
            imageView = i;
        }

        @Override
        protected Bitmap doInBackground(String... params){
            Bitmap bitmap = null;
            HttpURLConnection connection = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection)url.openConnection();
                connection.setConnectTimeout(5000);
                int reponse = connection.getResponseCode();
                if (reponse == HttpURLConnection.HTTP_OK) {
                    try (InputStream inputStream = connection.getInputStream()) {
                        bitmap = BitmapFactory.decodeStream(inputStream);
                        bitmaps.put(params[0], bitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                        ;
                    }
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
            finally {
                connection.disconnect();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap){
            imageView.setImageBitmap(bitmap);
        }
    }



}