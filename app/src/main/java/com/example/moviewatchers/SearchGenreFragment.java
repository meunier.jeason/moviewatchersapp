package com.example.moviewatchers;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moviewatchers.adapter.FilmAdapter;
import com.example.moviewatchers.models.Film;
import com.example.moviewatchers.models.Genre;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;


public class SearchGenreFragment extends Fragment {
    private List<Film> filmList = new ArrayList<>();
    private FilmAdapter filmAdapter = new FilmAdapter(filmList);
    private LinearLayoutManager linearLayoutManager;
    private int page=1;
    private int maxPage;
    private RecyclerView recyclerView;
    private List<Integer> genreId = new ArrayList<>();
    private List<String> genreName = new ArrayList<>();
    private int id_genre = 28; // par defaut genre action
    private int selected = 0;
    private TextView tvGenre;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return inflater.inflate(R.layout.homefragment,container,false);


        View view = inflater.inflate(R.layout.searchgenrefragment, container, false);
        recyclerView = view.findViewById(R.id.recyclerviewFilms);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(filmAdapter);
        findGenres(recyclerView,1, id_genre);
        getGenres();
        // Scroll continu avec appel a l'api
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == filmList.size()-1){
                    if (maxPage == 0 || page < maxPage){
                        page = page + 1;
                        findGenres(recyclerView,page,id_genre);


                    }

                }
            }
        });
        tvGenre = view.findViewById(R.id.selectedGenre);
        tvGenre.setText("Action");
        Button button = view.findViewById(R.id.buttonGenre);
        button.setOnClickListener(v -> {
            loadModal();
        });
        return view;

    }

    private void loadModal() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Choisir un genre");

        // add a radio button list
        builder.setSingleChoiceItems(genreName.toArray(new String[genreName.size()]), selected, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d("MESLOGS","Genre selectionné: "+genreName.get(which)+" avec id :"+genreId.get(which));
                id_genre = genreId.get(which);
                selected = which;

            }
        });
        // add OK and Cancel buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                page = 1;
                filmList.clear();
                Log.d("MESLOGS",""+id_genre);
                linearLayoutManager.scrollToPositionWithOffset(0, 0);
                tvGenre.setText(genreName.get(selected));
                findGenres(recyclerView,page,id_genre);

            }
        });
        builder.setNegativeButton("Annuler", null);

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void getGenres() {
        URL url = createURLGenre();
        GetGenreTask getGenreTask = new GetGenreTask(getActivity());
        getGenreTask.execute(url);
    }

    public void findGenres(View view, int page, int id_genre){
        URL url = createURL("discover/movie","sort_by=popularity.desc&include_adult=false&include_video=false&page="+page+"&with_genres="+id_genre);
        if(url != null){
            GetFilmTask getFilmTask = new GetFilmTask(getActivity());
            getFilmTask.execute(url);
        } else {
            Toast.makeText(getActivity(), "Url Invalide", Toast.LENGTH_LONG).show();
        }
    }

    private URL createURLGenre() {
        String apiKey = getString(R.string.apikey);
        String baseUrl = getString(R.string.apiurl);

        try{
            String urlString = baseUrl+"genre/movie/list?api_key="+apiKey+"&language=fr-FR";
            Log.d("MesLogs",urlString);
            return new URL(urlString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private URL createURL(String endpoint, String searchFilters) {
        String apiKey = getString(R.string.apikey);
        String baseUrl = getString(R.string.apiurl);

        try{
            String urlString = baseUrl + endpoint +"?api_key="+apiKey+"&language=fr-FR&"+searchFilters;
            Log.d("MesLogs",urlString);
            return new URL(urlString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private class GetFilmTask extends AsyncTask<URL,Void, JSONObject> {
        private Context context;

        public GetFilmTask(Context c){
            context = c;
        }


        @Override
        protected JSONObject doInBackground(URL... params) {
            HttpsURLConnection connection = null;
            try {
                connection = (HttpsURLConnection)params[0].openConnection();
                connection.setConnectTimeout(5000);
                int res = connection.getResponseCode();
                if (res == HttpsURLConnection.HTTP_OK){
                    StringBuilder builder = new StringBuilder();
                    try(BufferedReader reader = new BufferedReader(
                            new InputStreamReader(connection.getInputStream())
                    )){
                        String line;
                        while ((line = reader.readLine()) != null){
                            builder.append(line);
                        }
                    } catch (IOException e){
                        e.printStackTrace();
                    }
                    return new JSONObject(builder.toString());
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject film){
            if(film != null){
                convertJsonToArrayList(film);
                filmAdapter.notifyDataSetChanged();
            }

        }

        private void convertJsonToArrayList(JSONObject film) {
            try{
                JSONArray list = film.getJSONArray("results");
                //Log.d("MesLogs", list.toString());
                for (int i=0; i<list.length();i++){
                    JSONObject filmInfo = list.getJSONObject(i);
                    //Log.d("MesLogs",filmInfo.toString());
                    int id = filmInfo.getInt("id");
                    String title = filmInfo.getString("title");
                    String overview = filmInfo.getString("overview");
                    String posterPath = filmInfo.getString("poster_path");
                    filmList.add(new Film(
                            id,
                            title,
                            overview,
                            posterPath
                    ));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private class GetGenreTask extends AsyncTask<URL,Void, JSONObject> {
        private Context context;

        public GetGenreTask(Context c){
            context = c;
        }


        @Override
        protected JSONObject doInBackground(URL... params) {
            HttpsURLConnection connection = null;
            try {
                connection = (HttpsURLConnection)params[0].openConnection();
                connection.setConnectTimeout(5000);
                int res = connection.getResponseCode();
                if (res == HttpsURLConnection.HTTP_OK){
                    StringBuilder builder = new StringBuilder();
                    try(BufferedReader reader = new BufferedReader(
                            new InputStreamReader(connection.getInputStream())
                    )){
                        String line;
                        while ((line = reader.readLine()) != null){
                            builder.append(line);
                        }
                    } catch (IOException e){
                        e.printStackTrace();
                    }
                    return new JSONObject(builder.toString());
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject genre){
            if(genre != null){
                convertJsonToArrayList(genre);
            }

        }

        private void convertJsonToArrayList(JSONObject genre) {
            try{
                JSONArray list = genre.getJSONArray("genres");
                //Log.d("MesLogs", list.toString());
                for (int i=0; i<list.length();i++){
                    JSONObject genreInfo = list.getJSONObject(i);
                    //Log.d("MesLogs",filmInfo.toString());
                    int id = genreInfo.getInt("id");
                    String name = genreInfo.getString("name");
                    genreId.add(id);
                    genreName.add(name);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

}
